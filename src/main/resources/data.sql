-- Insert Data into default user details
INSERT INTO users (id, username, password, role, enabled)
VALUES (1, 'user',
        '$2a$10$uuN9G0XuKRdhcY8AiabTlO5rhSGEN3jEn62b4Ul1JSUSaQofiv6b2',
        'ROLE_USER', 'TRUE');

INSERT INTO users (id, username, password, role, enabled)
VALUES (2, 'admin',
        '$2a$10$htVgh66BTs0Hlydyd/qMJO2DBVDRNMSCrKSKxCs4zvQuc66AGY3qa',
        'ROLE_ADMIN', 'TRUE');


-- Insert Data into INVENTORY_DETAILS Table
INSERT INTO INVENTORY_DETAILS VALUES (1, 'Chips', 200);
INSERT INTO INVENTORY_DETAILS VALUES (2, 'Hide & Seek Biscuit', 500);
INSERT INTO INVENTORY_DETAILS VALUES (3, 'Apple', 200);
INSERT INTO INVENTORY_DETAILS VALUES (4, 'Banana', 300);
INSERT INTO INVENTORY_DETAILS VALUES (5, 'Bandage', 1000);
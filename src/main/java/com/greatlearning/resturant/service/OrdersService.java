package com.greatlearning.resturant.service;

import com.greatlearning.resturant.entity.BillDetailsDto;
import com.greatlearning.resturant.entity.InventoryDetails;
import com.greatlearning.resturant.entity.Orders;
import com.greatlearning.resturant.entity.TotalSalesDto;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Optional;

public interface OrdersService {

    List<BillDetailsDto> generateTodayBills();

    TotalSalesDto getTotalSalesForThisMonth();

    void saveOrder(Authentication authentication, Integer selectedItem, Optional<InventoryDetails> item);

    List<Orders> getByUsername(Authentication authentication);
}

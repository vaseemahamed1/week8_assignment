package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;
import com.greatlearning.resturant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterController {

    @Autowired
    UserService userService;

    @PostMapping("/register")
    public Users register(@RequestBody UserDto userDto) {
        return userService.createUser(userDto);

    }
}

package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.BillDetailsDto;
import com.greatlearning.resturant.entity.TotalSalesDto;
import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;
import com.greatlearning.resturant.service.OrdersService;
import com.greatlearning.resturant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;


@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    UserService userService;

    @Autowired
    OrdersService ordersService;

    @GetMapping("/viewUser")
    public Users getUser(@RequestParam String userName) {
        return userService.getUser(userName);
    }

    @PostMapping("/createUser")
    public Users createUser(@RequestBody UserDto userDto) {
        return userService.createUser(userDto);
    }

    @PutMapping("/updateUser")
    public Users updateUser(@RequestParam String userName, @RequestBody UserDto userDto) {
        return userService.updateUser(userName, userDto);
    }

    @DeleteMapping("/deleteUser")
    @Transactional
    public String deleteUser(@RequestParam String userName) {
        userService.deleteUser(userName);
        return "user deleted";
    }

    @GetMapping("/generateTodayBills")
    public List<BillDetailsDto> generateTodayBills() {
        return ordersService.generateTodayBills();
    }

    @GetMapping("/getTotalSalesForCurrentMonth")
    public TotalSalesDto getTotalSalesForMonth() {
        return ordersService.getTotalSalesForThisMonth();
    }

}

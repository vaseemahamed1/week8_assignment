package com.greatlearning.resturant.entity;

import javax.persistence.*;
import java.sql.Date;


@Entity
public class AuditLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "description")
    private String description;

    public AuditLog() {

    }

    public AuditLog(Date createdDate, String description) {
        this.createdDate = createdDate;
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "AuditLog{" +
                "id=" + id +
                ", createdDate=" + createdDate +
                ", description='" + description + '\'' +
                '}';
    }
}

package com.greatlearning.resturant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week8AssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week8AssignmentApplication.class, args);
	}

}
